var args = $.args;

function closeWindow() {
	$.signin.close();
}

function showHidePassword() {
	if ($.password.passwordMask == false) {
		$.password.passwordMask = true;
		$.showHidePasswordImage.image = '/images/icon-password-show.png';
	} else {
		$.password.passwordMask = false;
		$.showHidePasswordImage.image = '/images/icon-password-hide.png';
	}
}

function checkemail(emailAddress) {
	var str = emailAddress;
	var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if (filter.test(str)) {
		testresults = true;
	} else {
		testresults = false;
	}
	return (testresults);
};

function openForgotPassword() {
	if (OS_IOS) {
		var dialog = Ti.UI.createAlertDialog({
			title : 'Restablecer contraseña',
			message : 'Por favor, introduce tu correo electrónico',
			hintText : 'correo electrónico',
			style : Ti.UI.iOS.AlertDialogStyle.PLAIN_TEXT_INPUT,
			cancel : 1,
			buttonNames : ['Ok', 'Cancelar']
		});
	} else if (OS_ANDROID) {
		var textfield = Ti.UI.createTextField();
		var dialog = Ti.UI.createAlertDialog({
			title : 'Restablecer contraseña',
			message : 'Por favor, introduce tu correo electrónico',
			hintText : 'correo electrónico',
			androidView : textfield,
			cancel : 1,
			buttonNames : ['Ok', 'Cancelar']
		});
	}
	dialog.addEventListener('click', function(e) {
		if (e.index == 0) {
			if (OS_IOS) {
				var email = e.text;
			} else if (OS_ANDROID) {
				var email = textfield.value;
			}

			$.activityIndicatorView.show();
			$.activityIndicator.show();

			var requestParams = {
				email : email
			};

			Ti.API.info('requestParams ' + requestParams);

			var url = Alloy.CFG.APIurl + 'user/password';

			var xhr = Ti.Network.createHTTPClient({
				timeout : 5000
			});
			xhr.onload = function() {

				Ti.API.info('response from user/password ' + this.responseText);

				try {
					var jsonObject = JSON.parse(this.responseText);

					if (jsonObject.status == 'success') {
						Ti.API.info('success');

						var dialog = Ti.UI.createAlertDialog({
							cancel : 0,
							buttonNames : ['Ok'],
							message : 'Se ha enviado un correo con un enlace para recuperar su contraseña.',
							title : 'Bien'
						});
						dialog.show();

					} else if (jsonObject.status == 'error') {
						var error_dialog = Ti.UI.createAlertDialog({
							cancel : 0,
							buttonNames : ['Ok'],
							message : jsonObject.message,
							title : '¡Ups!'
						});
						error_dialog.show();
					}

				} catch(e) {
					Ti.API.error("Caught: " + e);

					$.activityIndicatorView.hide();
					$.activityIndicator.hide();

					var error_dialog = Ti.UI.createAlertDialog({
						cancel : 0,
						buttonNames : ['Ok'],
						message : e.message,
						title : '¡Ups!'
					});
					error_dialog.show();
				}

				$.activityIndicatorView.hide();
				$.activityIndicator.hide();
			};
			xhr.onerror = function(e) {
				Ti.API.info('onerror ' + e);

				$.activityIndicatorView.hide();
				$.activityIndicator.hide();

				var error_dialog = Ti.UI.createAlertDialog({
					cancel : 0,
					buttonNames : ['Ok'],
					message : 'There might be a problem, try again',
					title : '¡Ups!'
				});
				error_dialog.show();
			};
			xhr.open('POST', url);
			xhr.setRequestHeader('Api-Key', Alloy.CFG.APIkey);
			xhr.send(requestParams);

		}
	});
	dialog.show();
}

function passwordFocus() {
	$.password.focus();
}

function signIn() {
	if ($.email.getValue() == '' || $.password.getValue() == '') {
		var error_dialog = Ti.UI.createAlertDialog({
			cancel : 0,
			buttonNames : ['Ok'],
			message : 'Debe de completar todos los campos',
			title : '¡Ups!'
		});
		error_dialog.show();
	} else if (checkemail($.email.getValue()) == false) {
		var error_dialog = Ti.UI.createAlertDialog({
			cancel : 0,
			buttonNames : ['Ok'],
			message : 'Debe de proporcionar un correo electrónico válido',
			title : '¡Ups!'
		});
		error_dialog.show();
	} else {
		$.activityIndicatorView.show();
		$.activityIndicator.show();

		requestParams = {
			email : $.email.value,
			password : $.password.value
		};

		urlsignin = Alloy.CFG.APIurl + '/access/associates/signin';
		xhrsignin = Ti.Network.createHTTPClient({
			timeout : 5000
		});

		xhrsignin.onload = function() {
			Ti.API.info('ws:access/signin ' + this.responseText);
			response = JSON.parse(this.responseText);

			if (response.status == 'success') {
				Titanium.App.Properties.setInt('loggedIn', 1);
				Titanium.App.Properties.setString("loggedInType", "email");
				Titanium.App.Properties.setInt('userID', response.data[0].user_id);
				Titanium.App.Properties.setString('userFullName', response.data[0].full_name);
				Titanium.App.Properties.setString("userEmail", response.data[0].email);
				Titanium.App.Properties.setInt('associateID', response.data[0].associate_id);
				Alloy.Globals.master = Alloy.createController("/master").getView();
				Alloy.Globals.master.open();
				//Alloy.Globals.main.close();
			} else {
				var error_dialog = Ti.UI.createAlertDialog({
					cancel : 0,
					buttonNames : ['Ok'],
					message : 'Verifica que el correo y la contraseña sean correctos e intenta de nuevo',
					title : '¡Ups!'
				});
				error_dialog.show();
			}
		};

		xhrsignin.onerror = function(e) {
			$.activityIndicatorView.hide();
			$.activityIndicator.hide();

			var error_dialog = Ti.UI.createAlertDialog({
				cancel : 0,
				buttonNames : ['Ok'],
				message : e.message,
				title : '¡Ups!'
			});
			error_dialog.show();
		};

		xhrsignin.open('POST', urlsignin);
		xhrsignin.setRequestHeader('Api-Key', Alloy.CFG.APIkey);
		xhrsignin.send(requestParams);
	}
}

function postlayout() {
	$.email.focus();
}

function open() {
	if (OS_ANDROID) {
		var activity = $.signin.getActivity();
		activity.actionBar.displayHomeAsUp = false;
		activity.actionBar.onHomeIconItemSelected = function() {
			Ti.API.info('home click');
			closeWindow();
		};

		var abx = require('com.alcoapps.actionbarextras');
		abx.statusbarColor = '#17b38f';
		abx.backgroundColor = '#19a585';
		abx.title = 'Entrar';
		abx.titleFont = "SanFranciscoText-Medium.otf";
		abx.subtitleFont = "SanFranciscoText-Medium.otf";
		abx.titleColor = "#fff";
		abx.subtitleColor = "#fff";
		abx.setHomeAsUpIcon("/nav-close-white.png");
	}

	$.showHidePassword.addEventListener('click', showHidePassword);

	$.forgotPassowrd.addEventListener('click', openForgotPassword);
	$.signinButton.addEventListener('click', signIn);
	$.email.addEventListener('return', passwordFocus);

	$.signin.addEventListener('close', close);

}

function close() {
	$.showHidePassword.removeEventListener('click', showHidePassword);

	$.forgotPassowrd.removeEventListener('click', openForgotPassword);
	$.signinButton.removeEventListener('click', signIn);
	$.email.removeEventListener('return', passwordFocus);

	$.signin.removeEventListener('open', open);
	$.signin.removeEventListener('close', close);
}

$.signin.addEventListener('open', open);

Ti.App.addEventListener('signin:closeWindow', closeWindow);
