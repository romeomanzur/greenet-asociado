var args = $.args;
var Map = require('ti.map');

var data;
var platformWidth = Ti.Platform.displayCaps.platformWidth;
var latitude;
var longitude;
var address;

function closeWindow() {
	$.order.close();
}

function openMap() {
	var map = Alloy.createController("map", {
		address : address,
		latitude : latitude,
		longitude : longitude
	}).getView();
	if (OS_IOS) {
		Alloy.Globals.tabOrders.openWindow(map);
	} else if (OS_ANDROID) {
		map.open();
	}
}

function loadReport() {
	$.activityIndicatorView.show();
	$.activityIndicator.show();

	var url = Alloy.CFG.APIurl + '/orders/' + args.order_id;
	Titanium.API.info('url ' + url);
	var xhr = Ti.Network.createHTTPClient();
	xhr.onload = function() {
		Ti.API.info('response from: /orders/: ' + this.responseText);

		var jsonObject = JSON.parse(this.responseText);

		if (jsonObject.status == 'success') {
			if (jsonObject.data) {

				data = jsonObject.data;

				$.id.text = 'Numero de orden: ' + data.order_id;

				var a = data.date_created.split(" ");
				var b = a[0].split("-");
				var c = a[1].split(":");
				$.published.text = b[2] + '/' + b[1] + '/' + b[0];

				datestart = data.period.date_start.split(" ");
				datestart0 = datestart[0].split("-");
				datestart1 = datestart[0].split(":");

				dateend = data.period.date_end.split(" ");
				dateend0 = dateend[0].split("-");
				dateend1 = dateend[0].split(":");
				
				var typeServices = data.period.is_one_day;
				
				if (typeServices == 0) {
					$.categoryView.backgroundColor = '#19a585';
					$.categoryLabel.text = 'Paquete';
				}else if (typeServices == 1) {
					$.categoryView.backgroundColor = '#19a585';
					$.categoryLabel.text = 'Servicio Ocacional';
				};

				$.published.text = 'De ' + datestart0[2] + '/' + datestart0[1] + '/' + datestart0[0] + ' a ' + dateend0[2] + '/' + dateend0[1] + '/' + dateend0[0];

				var desc = data.package.description.split("\\r\\n");
				$.desc1.text = '*' + desc[0];
				$.desc2.text = '*' + desc[1];
				$.desc3.text = '*' + desc[2];

				$.providername.text = data.associate.name;

				if (data.category_id == 1) {
					$.category.color = '#1c4da1';
				} else if (data.category_id == 2) {
					$.category.color = '#ff008d';
				} else if (data.category_id == 3) {
					$.category.color = '#00aa4f';
				} else if (data.category_id == 4) {
					$.category.color = '#ffc61b';
				} else if (data.category_id == 5) {
					$.category.color = '#4e5665';
				}

				$.category.text = data.category;
				$.type.text = 'Tipo de servicio: ' + data.type.name;
				$.pricelabel.text = '$' + data.total_cost;

				$.address.text = data.location.address;

				if (data.status == 1) {
					$.statusView.backgroundColor = '#ed1c24';
					$.statusLabel.text = 'En espera';
				} else if (data.status == 2) {
					$.statusView.backgroundColor = '#ffb301';
					$.statusLabel.text = 'En atención';
				} else if (data.status == 3) {
					$.statusView.backgroundColor = '#2cb900';
					$.statusLabel.text = 'Atendido';
				}

				if (OS_IOS) {
					var Snapshotter = Map.createSnapshotter({
						mapType : Map.NORMAL_TYPE,
						region : {
							latitude : data.location.latitude,
							longitude : data.location.longitude,
							latitudeDelta : 0.002,
							longitudeDelta : 0.002,
						},
						size : {
							width : platformWidth,
							height : 200
						}
					});

					Snapshotter.takeSnapshot({
						success : function(e) {
							$.map.image = e.image;
							$.pin.visible = true;
						},
						error : function(e) {
							Ti.API.error("The snapshot could not be taken: " + e.error);
						}
					});
				};

				latitude = data.location.latitude;
				longitude = data.location.longitude;
				address = data.location.address;

				var fadeIn = Ti.UI.createAnimation({
					curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
					opacity : 1,
					duration : 300
				});

				$.scrollview.animate(fadeIn, function() {
					$.scrollview.opacity = 1;
				});

			} else {

			}
		}

		$.activityIndicatorView.hide();
		$.activityIndicator.hide();

	};
	xhr.onerror = function(e) {
		Ti.API.info(e);

		$.activityIndicatorView.hide();
		$.activityIndicator.hide();
		$.errorView.opacity = 1;

	};
	xhr.open('GET', url);
	xhr.setRequestHeader('Api-Key', Alloy.CFG.APIkey);
	xhr.setRequestHeader('User-Id', Ti.App.Properties.getString('userID'));
	xhr.setRequestHeader('User-Token', Ti.App.Properties.getString('userToken'));
	xhr.send();
}

function open() {
	if (OS_ANDROID) {

		var activity = $.order.getActivity();

		activity.actionBar.displayHomeAsUp = true;
		activity.actionBar.onHomeIconItemSelected = function() {
			Ti.API.info('home click');
			closeWindow();
		};

		var abx = require('com.alcoapps.actionbarextras');
		abx.statusbarColor = '#19a585';
		abx.backgroundColor = '#19a585';
		abx.title = 'Servicio';
		abx.titleFont = "SanFranciscoText-Medium.otf";
		abx.subtitleFont = "SanFranciscoText-Medium.otf";
		abx.titleColor = "#fff";
		abx.subtitleColor = "#fff";
		abx.setHomeAsUpIcon("/images/nav-close-white.png");

	}
	loadReport();

	if (OS_IOS) {
		$.closeButton.addEventListener('click', closeWindow);
	}

	$.mapview.addEventListener('click', openMap);

	$.order.addEventListener('close', close);
}

function close() {
	if (OS_IOS) {
		$.closeButton.removeEventListener('click', closeWindow);
	}

	$.mapview.removeEventListener('click', openMap);

	$.order.removeEventListener('open', open);
	$.order.removeEventListener('close', close);
}

$.order.addEventListener('open', open);