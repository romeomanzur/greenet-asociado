var args = $.args;
var confirmLogout = Ti.UI.createAlertDialog({
	message : '¿Desea cerrar sesión?',
	title : 'Salir',
	buttonNames : ['Aceptar', 'Cancelar']
});

function closeWindow() {
	confirmLogout.show();
}

function logout(e) {
	if (e.index == 0) {
		Titanium.App.Properties.removeAllProperties();
		signin = Alloy.createController("signin").getView();
		signin.open();
	};
}

function tiketSupport(){
	var tiket = Alloy.createController("tiket", {
		
	}).getView();
	if (OS_IOS) {
		Alloy.Globals.tabSettings.openWindow(tiket);
	} else if (OS_ANDROID) {
		summary.open();
	}
}

function open() {
	//$.support.addEventListener('click', tiketSupport);
	$.signout_button.addEventListener('click', closeWindow);
	confirmLogout.addEventListener('click', logout);
}

function close() {
	//$.support.removeEventListener('click', tiketSupport);
	$.signout_button.removeEventListener('click', closeWindow);
	confirmLogout.removeEventListener('click', logout);
}

$.settings.addEventListener('open', open);
