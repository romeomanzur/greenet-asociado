var args = $.args;
var bdrFilter = 'all';
function openOrdery(e) {
	var summary = Alloy.createController("order", {
		order_id : this.orderId
	}).getView();
	if (OS_IOS) {
		Alloy.Globals.tabOrders.openWindow(summary);
		//$.orders.openWindow(summary);
	} else if (OS_ANDROID) {
		summary.open();
	}
}

function getOrders() {
	$.scrollView.removeAllChildren();
	urlassociate = Alloy.CFG.APIurl + '/orders/list/associate/' + bdrFilter + '/' + Ti.App.Properties.getInt('associateID');
	xhrassociate = Ti.Network.createHTTPClient({
		timeout : 5000
	});

	Titanium.API.info('urlassociate ' + urlassociate);

	xhrassociate.onload = function() {
		Ti.API.info('ws:orders/list/associate ' + this.responseText);
		response = JSON.parse(this.responseText);

		if (response.status == 'success') {
			if (response.data.length > 0) {
				for (var x = 0,
				    len = response.data.length; x < len; x++) {

					var order = response.data[x];

					var orderView = Alloy.createController("orderview", {
						report_id : order.order_id,
						category_id : order.category_id,
						category_name : order.category,
						type_id : order.type_id,
						type_name : order.type,
						associate : order.associate,
						location : order.location,
						latitude : order.latitude,
						longitude : order.longitude,
						date_created : order.date_created,
						address : order.address,
						period: order.period.is_one_day
					}).getView();

					orderView.addEventListener('click', openOrdery);

					$.scrollView.add(orderView);
				}
			} else {
				var bdrNoOrders = '';
				if (bdrFilter == 'active') {
					bdrNoOrders = 'activos';
				} else if (bdrFilter == 'news') {
					bdrNoOrders = 'nuevos';
				} else if (bdrFilter == 'done') {
					bdrNoOrders = 'finalizados';
				}
				;

				var labelNoOrders = Titanium.UI.createLabel({
					color : '#939393',
					text : 'No ahí servicios ' + bdrNoOrders,
					width : '80%',
					height : Ti.UI.SIZE,
					zIndex : 10,
					textAlign : 'center',
					font : {
						fontSize : 22
					},
					top : '30%'
				});
				$.scrollView.add(labelNoOrders);
			}
		} else {
			var error_dialog = Ti.UI.createAlertDialog({
				cancel : 0,
				buttonNames : ['Ok'],
				message : response.message,
				title : '¡Ups!'
			});
			error_dialog.show();
		}
	};

	xhrassociate.onerror = function(e) {
		$.activityIndicatorView.hide();
		$.activityIndicator.hide();

		var error_dialog = Ti.UI.createAlertDialog({
			cancel : 0,
			buttonNames : ['Ok'],
			message : e.message,
			title : '¡Ups!'
		});
		error_dialog.show();
	};

	xhrassociate.open('GET', urlassociate);
	xhrassociate.setRequestHeader('Api-Key', Alloy.CFG.APIkey);
	xhrassociate.setRequestHeader('User-Id', Ti.App.Properties.getInt('userID'));
	xhrassociate.setRequestHeader('User-Token', Ti.App.Properties.getString('userKey'));
	xhrassociate.send();
}

function filterOrders() {
	$.dialogfilter.show();
}

function selectFilter(e) {
	if (e.index == 0) {
		bdrFilter = 'active';
	} else if (e.index == 1) {
		bdrFilter = 'new';
	} else if (e.index == 2) {
		bdrFilter = 'done';
	} else if (e.index == 3) {
		bdrFilter = 'all';
	} else {

	}
	getOrders();
}

function refreshOrders() {
	setTimeout(function() {
		Ti.API.debug('Timeout');
		getOrders();
		$.rfc.endRefreshing();
	}, 2000);
}

function open() {
	getOrders();
	$.filter.addEventListener('click', filterOrders);
	$.dialogfilter.addEventListener('click', selectFilter);
	$.rfc.addEventListener('refreshstart', refreshOrders);
}

function close() {
	$.filter.removeEventListener('click', filterOrders);
	$.dialogfilter.removeEventListener('click', selectFilter);
	$.rfc.removeEventListener('click', refreshOrders);
}

$.orders.addEventListener('open', open);
