var args = $.args;
var photoHeight = 180;
var randfake = new Date();
var platformWidth = Ti.Platform.displayCaps.platformWidth;

var Map = require('ti.map');

$.orderview.orderId = args.report_id;

var a = args.date_created.split(" ");
var b = a[0].split("-");
var c = a[1].split(":");
$.published.text = b[2] + '/' + b[1] + '/' + b[0];

if (args.category_id == 1) {
	$.team_name.color = '#1c4da1';
	$.team_name.text = args.category_name;
} else if (args.category_id == 2) {
	$.team_name.color = '#ff008d';
	$.team_name.text = args.category_name;
} else if (args.category_id == 3) {
	$.team_name.color = '#00aa4f';
	$.team_name.text = args.category_name;
} else if (args.category_id == 4) {
	$.team_name.color = '#ffc61b';
	$.team_name.text = args.category_name;
} else if (args.category_id == 5) {
	$.team_name.color = '#4e5665';
	$.team_name.text = args.category_name;
}

if (args.period == 0) {
	$.categoryView.backgroundColor = '#19a585';
	$.categoryLabel.text = 'Paquete';
} else if (args.period == 1) {
	$.categoryView.backgroundColor = '#19a585';
	$.categoryLabel.text = 'Servicio Ocacional';
}
;

$.team_name.text = args.associate.name + ' (' + args.type_name.name + ')';
$.addresslabel.text = args.location.address;

if (OS_IOS) {
	var Snapshotter = Map.createSnapshotter({
		mapType : Map.NORMAL_TYPE,
		region : {
			latitude : args.location.latitude,
			longitude : args.location.longitude,
			latitudeDelta : 0.002,
			longitudeDelta : 0.002,
		},
		size : {
			width : platformWidth,
			height : 200
		}
	});
} else {
	var pincurrent = Map.createAnnotation({
		latitude : args.location.latitude,
		longitude : args.location.longitude
	});

	var mapview = Map.createView({
		mapType : Map.NORMAL_TYPE,
		region : {
			latitude : args.location.latitude,
			longitude : args.location.longitude,
			latitudeDelta : 0.01,
			longitudeDelta : 0.01
		},
		animate : false,
		regionFit : false,
		annotations : [pincurrent],
		touchEnabled : false
	});

	$.viewmap.add(mapview);
	$.photo.visible = false;
	$.pin.visible = false;
	$.imgdefault.visible = false;
}

if (OS_IOS) {
	Snapshotter.takeSnapshot({
		success : function(e) {
			$.photo.image = e.image;
			$.pin.visible = true;
		},
		error : function(e) {
			Ti.API.error("The snapshot could not be taken: " + e.error);
		}
	});
};

