
if (Ti.App.Properties.getInt('loggedIn') === 1) {
    Alloy.Globals.master = Alloy.createController("/master").getView();
    Alloy.Globals.master.open();
} else {
    Alloy.Globals.main = Alloy.createController("/signin").getView();
    Alloy.Globals.main.open();
}
