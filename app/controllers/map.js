var args = arguments[0] || {};
var Map = require('ti.map');
var mapview;

function closeWindow() {
    $.location.close();
}

function open() {
    //load mapview
    mapview = Map.createView({
        mapType : Map.NORMAL_TYPE,
        region : {
            latitude : args.latitude,
            longitude : args.longitude,
            latitudeDelta : 0.02,
            longitudeDelta : 0.02,
            userLocation : false,
            visible : true
        },

    });

    var annotations = [];
    var annotation;

    annotation = Map.createAnnotation({
        latitude : args.latitude,
        longitude : args.longitude,
        image : '/images/icon-report-pin.png'
    });

    annotations.push(annotation);

    mapview.addAnnotations(annotations);

    $.container.add(mapview);

    var fadeIn = Ti.UI.createAnimation({
        curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
        opacity : 1,
        duration : 300,
    });

    $.container.animate(fadeIn, function() {
        $.container.opacity = 1;
    });
    
    $.closeButton.addEventListener('click', closeWindow);
    $.location.addEventListener('close', close);
}

function close() {
    $.closeButton.removeEventListener('click', closeWindow);
    $.location.removeEventListener('open', open);
    $.location.removeEventListener('close', close);
}

$.location.addEventListener('open', open);